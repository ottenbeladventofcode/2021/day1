﻿using System;
using System.Collections.Generic;

namespace DayOne
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length != 2)
                {
                    throw new Exception("This program takes two inputs the first denoting which algorithm to use and the second the file path of a file containing a list of measurements");
                }

                int algorithm = int.Parse(args[0]);
                string[] inputs = System.IO.File.ReadAllLines(args[1]);

                List<int> measurements = new List<int>();

                for (int i = 0; i < inputs.Length; i++)
                {
                    if (int.TryParse(inputs[i], out int input))
                    {
                        measurements.Add(input);
                    }
                }

                DateTime start = DateTime.Now;
                int result = -1;

                if (algorithm == 0)
                {
                    result = Logic.Sonar(measurements);
                }
                else
                {
                    result = Logic.AdvancedSonar(measurements);
                }
                
                DateTime end = DateTime.Now;
                Console.WriteLine(string.Format("There are {0} measurements that are larger than the previous measurement.", result));
                Console.WriteLine(end.Subtract(start).ToString());
            }
            catch (Exception)
            {
                Console.Write("Unexpected error");
            }
        }
    }

    public static class Logic
    {
        public static int Sonar(List<int> measurements)
        {
            int result = 0;

            if (measurements.Count == 0)
            {
                return result;
            }

            int previous = measurements[0];
            for (int i = 1; i < measurements.Count; i++)
            {
                if (previous < measurements[i])
                {
                    result++;
                }
                previous = measurements[i];
            }

            return result;
        }

        public static int AdvancedSonar(List<int> measurements)
        {
            int result = 0;

            if (measurements.Count < 4)
            {
                return result;
            }

            for (int i = 3; i < measurements.Count; i++)
            {
                int previous = measurements[i - 1] + measurements[i - 2] + measurements[i - 3];
                int current = measurements[i]  + measurements[i - 1] + measurements[i - 2];

                if (previous < current)
                {
                    result++;
                }
            }

            return result;
        }
    }
}
