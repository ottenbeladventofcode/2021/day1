using DayOne;
using System;
using System.Collections.Generic;
using Xunit;

namespace DayOneTest
{
    public class UnitTest1
    {
        [Fact]
        public void Problem1Test()
        {
            List<int> measurements = new List<int>() { 199, 200, 208, 210, 200, 207, 240, 269, 260, 263 };
            const int expectedResult = 7;

            int result = Logic.Sonar(measurements);
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Problem2Test()
        {
            List<int> measurements = new List<int>() { 607, 618, 618, 617, 647, 716, 769, 792 };
            const int expectedResult = 5;

            int result = Logic.AdvancedSonar(measurements);
            Assert.Equal(expectedResult, result);
        }
    }
}
